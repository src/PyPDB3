### local banks
# local PDB version
DFLT_PDB_ROOT = "/scratch/banks/pdb"
GDFLTPDBDIR = "/scratch/banks/pdb/data/structures"
# local Astral Scop version
GDFLTSCOPDIR = "/nfs/freetown/banks/biomaj/astral/astral_2009-06-03/flat/"
# local CATH version
GDFLTCATHDIR = "/nfs/banks/CATH/current/pdb"

### hydrogens addition software
# HAAD software
HAADBIN = "/usr/local/softs/HAAD/HAAD"
# Reduce software
REDUCEBIN = "/usr/local/softs/reduce/reduce"
