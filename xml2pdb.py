#!/usr/bin/env python3

# pip install xmlschema --user
import xmlschema

import PyPDB.PyPDB as PDB
import PyPDB.Geo3DUtils as geo3d

import sys

def load_xml_component(fname):
    schema = xmlschema.XMLSchema("http://pdbml.pdb.org/schema/pdbx-v40.xsd")
    # entry_dict = schema.to_dict('./SER.xml')
    entry_dict = schema.to_dict(fname)
    infos   = entry_dict['PDBx:chem_compCategory']['PDBx:chem_comp'][0]
    content = entry_dict['PDBx:chem_comp_atomCategory']
    return infos, content

def search_atom(content, atmName):
    for atm in content['PDBx:chem_comp_atom']:
        if atm['@atom_id'] == atmName:
            return  atm
    return None

def is_amino_acid(content):
    if ( search_atom(content, "N") is not None ) and \
       ( search_atom(content, "N") is not None ) and \
       ( search_atom(content, "C") is not None ) and \
       ( search_atom(content, "O") is not None ):
        return True
    return False

def comp_id(content):
    atm = content['PDBx:chem_comp_atom'][0]
    return atm['@comp_id']

def to_pdb(content):
    atmNum = 0
    res = []
    for atm in content['PDBx:chem_comp_atom']:
        x= PDB.atmLine()
        x.header("ATOM")
        x.atmName(atm['@atom_id'])
        x.resName(atm['@comp_id'])
        x.chnLbl('A')
        atmNum += 1
        x.atmNum("%5d" % atmNum)
        x.setcrds(float(atm['PDBx:pdbx_model_Cartn_x_ideal']['$']),
                  float(atm['PDBx:pdbx_model_Cartn_y_ideal']['$']),
                  float(atm['PDBx:pdbx_model_Cartn_z_ideal']['$']))
        res.append(x)
    aList = PDB.atmList(res)
    n     = aList.theAtm("N" ).xyz()
    ca    = aList.theAtm("CA").xyz()
    c     = aList.theAtm("C" ).xyz()
    wList = geo3d.world2Peptide(n, ca, c, aList, scOnly = False)
    wres  = PDB.atmList(wList)
    wres  = PDB.residue(wres)
    return wres
    
if __name__ == "__main__":
    infos, content = load_xml_component(sys.argv[1])
    # print(infos)
    comp_name = infos['PDBx:name']
    if is_amino_acid(content):
        print(comp_id(content), comp_name )
        # print(to_pdb(content))
